---
layout: handbook-page-toc
title: "Product Design Pairs"
description: "Product designer pairs rotation schedule"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This is the rotation schedule for FY23-Q1 and Q2 (February - July, 2022). Learn more about [Pair Designing](/handbook/engineering/ux/how-we-work/#pair-designing).

[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Product Designer       | Design Pair      |
| ---------------------- | ---------------- |
| Alexis Ginsberg        | Nick Brandt      |
| Ali Ndlovu             | Sunjung Park     |
| Austin Regnery         | Annabel Gray     |
| Camellia Yang          | Matt Nearents\*  |
| Daniel Mora            | Andy Volpe       |
| Emily Bauman           | Becka Lippert    |
| Katie Macoy            | Amelia Bauerly   |
| Kevin Comoli           | Dan Mizzi-Harris |
| Michael Fangman        | Emily Sybrant    |
| Michael Le             | Libor Vanc       |
| Nadia Sotnikova        | Jeremy Elder     |
| Nick Leonard           | Phillip Joyce    |
| Pedro Moreira da Silva | Gina Doyle\*     |
| Sascha Eggenberger     | Mike Nichols     |
| Veethika Mishra        | Matej Latin      |

### *Temporary pairing

The following Product Designers are temporarily paired while their original pairs are on extended PTO.

| Product Designer       | Period | Design Pair                 |
|------------------------|-----------|-----------------------------|
| Matt Nearents          | April-May | Gina Doyle                  |
