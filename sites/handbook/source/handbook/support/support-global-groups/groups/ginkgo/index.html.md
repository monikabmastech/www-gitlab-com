---
layout: handbook-page-toc
title: Home Page for Support's Ginkgo Group
description: Home Page for Support's Ginkgo Group
---

<!-- Search for all occurrences of NAME and replace them with the group's name.
     Search for all occurrences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Ginkgo group

Introductory text, logos, or whatever the group wants here

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ginkgo resources

- Our Slack Channel: [spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G)
- Our Team: [Ginkgo Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=ginkgo)

## Ginkgo workflows and processes

#### Daily Crush Sessions

EMEA/AMER friendly crush sessions are held every weekday from 14:30 to 15:30 UTC.

**Goal**:
 
Clear out FRTs and NRTs as a group. FRT taking priority.

**Guidelines**:
 
Unassigned tickets will be assigned based on a round robin of the attendants, their support region, and current capacity. The standard [Working on Tickets](../../../workflows/working-on-tickets.html) workflow still applies. 

These are just guidelines. Someone overloaded may opt out of the round robin and remain in the crush session to assist.

**Location**:

These meetings will be held in the Gingko Collab Room, which can be found in the bookmarks of Our Slack Channel, [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G).
